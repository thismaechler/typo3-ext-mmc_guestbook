var mmcGuestbook = {
		
	validateForm: function( formNode ){
		var formValid = true;
		// iterate form fields
		$( formNode ).find('.form-row .validate').each(function(){
			$(this).closest('.form-row').removeClass('error');
			var fieldValid = true;
			if( $(this).hasClass('required') && ! $(this).val() ){
				fieldValid = false;
			}
			if ( $(this).hasClass('email') && ! $(this).val().match(/^$|(\S+@\S+\.\S+)$/) ){
				fieldValid = false;			
			}
			if( $(this).hasClass('mts') && ! mmcGuestbook.validateMathTest( formNode ) ){
			  fieldValid = false;
			}
			if( !fieldValid ){
				$(this).closest('.form-row').addClass('error');
				if( formValid )
					$(this).focus();
				formValid = false;
			}
		});
		// return validation result
		return formValid;
	},
	
	validateMathTest: function( formNode ){
	  var $mts;
	  if( ($mts = $(formNode).find('#mmcgbform-mts') ).length == 0 )
	    return true; // no math test
	  var s = $mts.val();
    var l = $(formNode).find('#mmcgbform-mts-label').html();
    var expr = l.substr(0, l.search('='));
    var s2 = eval( expr ).toString();
    return (s == s2);
	}
	
}