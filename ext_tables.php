
<?php
defined('TYPO3') || die();

(function () {
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_mmcguestbook_domain_model_entry', 'EXT:mmc_guestbook/Resources/Private/Language/locallang_csh_tx_mmcguestbook_domain_model_entry.xlf');
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_mmcguestbook_domain_model_entry');
})();
