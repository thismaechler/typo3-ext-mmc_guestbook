<?php
defined('TYPO3') || die();

return [
    'ctrl' => [
        'title'    => 'LLL:EXT:mmc_guestbook/Resources/Private/Language/locallang_db.xlf:tx_mmcguestbook_domain_model_entry',
        'label' => 'message',
        'tstamp' => 'tstamp',
        'crdate' => 'crdate',
        'cruser_id' => 'cruser_id',
        'dividers2tabs' => TRUE,
        'default_sortby' => 'ORDER BY crdate DESC, lastname ASC, firstname ASC',

        'delete' => 'deleted',
        'enablecolumns' => [
            'disabled' => 'hidden',
        ],
        'searchFields' => 'firstname,lastname,email,url,place,message,comment,remote_ip,',
        'iconfile' => 'EXT:mmc_guestbook/Resources/Public/Icons/tx_mmcguestbook_domain_model_entry.gif'
    ],
    'types' => [
        '1' => ['showitem' => 'hidden, is_spam, crdate, firstname, lastname, email, url, place, message, comment, remote_ip, post_hash'],
    ],
    'columns' => [

        'hidden' => [
            'exclude' => true,
            'label' => 'LLL:EXT:core/Resources/Private/Language/locallang_general.xlf:LGL.enabled',
            'config' => [
                'type' => 'check',
                'renderType' => 'checkboxToggle',
                'items' => [
                    [
                        0 => '',
                        1 => '',
                        'invertStateDisplay' => true
                    ]
                ],
            ]
        ],

        'is_spam' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:mmc_guestbook/Resources/Private/Language/locallang_db.xlf:tx_mmcguestbook_domain_model_entry.is_spam',
            'config' => [
                'type' => 'check',
            ],
        ],

        'crdate' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:mmc_guestbook/Resources/Private/Language/locallang_db.xlf:tx_mmcguestbook_domain_model_entry.crdate',
            'config' => [
                'type' => 'none',
                'format' => 'datetime',
                'size' => 10
            ],
        ],

        'firstname' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:mmc_guestbook/Resources/Private/Language/locallang_db.xlf:tx_mmcguestbook_domain_model_entry.firstname',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'lastname' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:mmc_guestbook/Resources/Private/Language/locallang_db.xlf:tx_mmcguestbook_domain_model_entry.lastname',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim,required'
            ],
        ],
        'email' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:mmc_guestbook/Resources/Private/Language/locallang_db.xlf:tx_mmcguestbook_domain_model_entry.email',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'url' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:mmc_guestbook/Resources/Private/Language/locallang_db.xlf:tx_mmcguestbook_domain_model_entry.url',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'place' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:mmc_guestbook/Resources/Private/Language/locallang_db.xlf:tx_mmcguestbook_domain_model_entry.place',
            'config' => [
                'type' => 'input',
                'size' => 30,
                'eval' => 'trim'
            ],
        ],
        'message' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:mmc_guestbook/Resources/Private/Language/locallang_db.xlf:tx_mmcguestbook_domain_model_entry.message',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 10,
                'eval' => 'trim,required'
            ]
        ],
        'comment' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:mmc_guestbook/Resources/Private/Language/locallang_db.xlf:tx_mmcguestbook_domain_model_entry.comment',
            'config' => [
                'type' => 'text',
                'cols' => 40,
                'rows' => 10,
                'eval' => 'trim'
            ]
        ],
        'remote_ip' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:mmc_guestbook/Resources/Private/Language/locallang_db.xlf:tx_mmcguestbook_domain_model_entry.remote_ip',
            'config' => [
                'type' => 'none',
                'size' => 20
            ],
        ],
        'post_hash' => [
            'exclude' => 0,
            'label' => 'LLL:EXT:mmc_guestbook/Resources/Private/Language/locallang_db.xlf:tx_mmcguestbook_domain_model_entry.post_hash',
            'config' => [
                'type' => 'none',
                'size' => 20
            ],
        ],

    ],
];
