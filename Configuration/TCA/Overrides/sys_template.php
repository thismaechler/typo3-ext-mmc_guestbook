<?php
defined('TYPO3') || die();

(function () {

  \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('mmc_guestbook', 'Configuration/TypoScript', 'MMC Guestbook');

})();
