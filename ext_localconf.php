<?php
defined('TYPO3') || die();

(function () {

	\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
		'MmcGuestbook',
		'Gb',
		[\MMC\MmcGuestbook\Controller\EntryController::class => 'list, new, create, setIsSpam'],
		// non-cacheable actions
		[\MMC\MmcGuestbook\Controller\EntryController::class => 'create, new, setIsSpam']
	);

})();
