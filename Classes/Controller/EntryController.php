<?php
namespace MMC\MmcGuestbook\Controller;

use TYPO3\CMS\Extbase\Http\ForwardResponse;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use Psr\Http\Message\ResponseInterface;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Matthias Mächler <maechler@mm-computing.ch>, mm-computing.ch
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
* EntryController
*/
class EntryController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController 
{

    /**
    * entryRepository
    *
    * @var \MMC\MmcGuestbook\Domain\Repository\EntryRepository
    */
    protected $entryRepository = NULL;


    public function __construct(\MMC\MmcGuestbook\Domain\Repository\EntryRepository $entryRepository)
    {
        $this->entryRepository = $entryRepository;
    }

    /**
    * action list
    *
    * @param integer $page
    * @param string $showMessage
    * @return \Psr\Http\Message\ResponseInterface
    */
    public function listAction($page=0, $showMessage=''): ResponseInterface  
    {
        $pageSize = intval($this->settings['pageSize']);
        $entries = $this->entryRepository->findAllLimitedNotHidden( $pageSize*$page, $pageSize );
        $totalCount = $this->entryRepository->countAll();
        $pageCount = ceil( $totalCount / $pageSize );
        $pager = array(
            'prevPage' => ($page - 1),
            'nextPage' => ($page < $pageCount - 1) ?  ($page + 1) : -1,
            'currentPage' => $page,
            'pages' => array(),
            'pageCount' => $pageCount
        );
        for( $i = 0; $i < $pageCount; $i++ )
            $pager['pages'][] = $i+1;
        $this->view->assign('showMessage', $showMessage );
        $this->view->assign('pager', $pager );
        $this->view->assign('entries', $entries);
        return $this->htmlResponse();
    }

    /**
    * action new
    *
    * @param \MMC\MmcGuestbook\Domain\Model\Entry $newEntry
    * @param boolean $mtError
    * @return \Psr\Http\Message\ResponseInterface
    */
    public function newAction(\MMC\MmcGuestbook\Domain\Model\Entry $newEntry = NULL, $mtError = FALSE ): ResponseInterface  
    {
        $timestamp = $this->getMsTstamp();
        $random = md5( mt_rand() . '-' . mt_rand() . '-' . mt_rand() );
        $botTrap['timestamp'] = $timestamp;
        $botTrap['random'] = $random;
        $botTrap['pHash'] = $this->getFormPostHash( $timestamp, $random );
        if( $this->settings['useMathTest'] ){
            $n1 = mt_rand(1,9);
            $n2 = mt_rand(1,9);
            $botTrap['mtError'] = $mtError; // set math test error class
            $botTrap['mtLabel'] = $this->getMtLabel($n1, $n2);
            $botTrap['mtsHash'] = $this->getMtsHash($n1, $n2);
        }
        $this->view->assign('newEntry', $newEntry);
        $this->view->assign('botTrap', $botTrap);
        if( $this->settings['scrambleForm'] ){
            return $this->htmlResponse( $this->getScrambledHTML( $this->view->render() ) );
        } else {
            return $this->htmlResponse($this->view->render());
        }
    }

    /**
    * action create
    *
    * @param \MMC\MmcGuestbook\Domain\Model\Entry $newEntry
    * @param array $bt //bot trap fields
    * @return \Psr\Http\Message\ResponseInterface
    */
    public function createAction(\MMC\MmcGuestbook\Domain\Model\Entry $newEntry, $bt = NULL): ResponseInterface  
    {
        $isBot = $this->checkBotTraps( $bt );
        $remoteIp = $_SERVER['REMOTE_ADDR'];
        $forbiddenIp = ( $this->settings['blockSpamIps'] && $this->entryRepository->isSpamIp( $remoteIp ) );
        $listMsg = '';
        // save entry if not a bot or forbidden IP
        if( !($isBot || $forbiddenIp) ){
            // set entry values
            $newEntry->setCrdate( time() );
            $newEntry->setRemoteIp( $remoteIp );
            $newEntry->setPostHash( $bt['pHash'] );
            $newEntry->setHidden( $this->settings['hideNewEntries'] );
            // store new entry
            $this->entryRepository->add($newEntry);
            $this->entryRepository->persistAll();
            // set message to show in frontend
            if( $this->settings['hideNewEntries'] )
                $listMsg = 'createdHidden';
            else
                $listMsg = 'created';
            // send review mail
            if( $this->settings['reviewNewEntries'] )
                $this->sendReviewMail( $newEntry );
        }
        if( $isBot ){ // bot or user could not solve the math test :)
            return (new ForwardResponse('new'))->withArguments(['newEntry' => $newEntry, 'mtError' => TRUE]);
        }
        // redirect to list
          return $this->redirect('list', NULL, NULL, array('showMessage' => $listMsg) );
    }

    /**
    * action setIsSpam
    *
    * @param string $entryUid
    * @param boolean $isSpam
    * @param string $hash
    * @return \Psr\Http\Message\ResponseInterface
    */
    public function setIsSpamAction($entryUid, $isSpam, $hash): ResponseInterface  
    {
        $entry = $this->entryRepository->findByUidIncludeHidden( $entryUid );
        $listMsg = '';
        if( $entry && ($this->getSetIsSpamHash($entry) == $hash) ){
            $entry->setIsSpam( $isSpam );
            $entry->setHidden( $isSpam );
            $this->entryRepository->update($entry);
            $this->entryRepository->persistAll();
            $listMsg = $isSpam ? 'setIsSpam' : 'setIsNoSpam';
        }
        return (new ForwardResponse('list'))
            ->withArguments(['showMessage' => $listMsg]);
    }

    /**
    * sendReviewMail
    *
    * @param \MMC\MmcGuestbook\Domain\Model\Entry $entry
    * @return boolean
    */
    private function sendReviewMail( $entry )
    {
        // make mailer instance
        $mail = GeneralUtility::makeInstance(\TYPO3\CMS\Core\Mail\MailMessage::class);
        // setup email components
        $recipients = array( $this->settings['reviewEmailAddress'] => '' );
        $from = array(
            $this->settings['reviewEmailSenderAddress'] => $this->settings['reviewEmailSenderName']
        );
        // use controller view (to have context)
        $this->view->setTemplate('Entry/ReviewMail.html');
        $this->view->assign( 'entry', $entry );
        $this->view->assign( 'setIsSpamHash', $this->getSetIsSpamHash($entry) );
        $subject = $this->settings['reviewEmailSubject'];
        $mailBody = $this->view->render();
        // send mail
        try{
            $mail->setFrom( $from )->setTo( $recipients )->setSubject( $subject );
            $mail->html($mailBody);
            $mail->send();
        } catch ( \Exception $e ){
            $this->addFlashMessage('error while sending mail: '.$e->getMessage() );
            return false;
        }
        if(! $mail->isSent()){
            $this->addFlashMessage('error while sending mail: mail could not be sent' );
            return false;
        }
        return true;
    }


    // checks the spambot traps, returns true if bot detected :
    // - h: empty
    // - pHash: md5( $bt['ts']+$bt['rnd']+[secret] )
    // - check for minimal form submission time ( $bt['ts'] )
    // - check if hash (pHash) has been used before (prevent POST-replay)
    // - check math test solution if enabled
    private function checkBotTraps( $bt )
    {
        return (
          // honeypot not empty?
            ($bt['h'] != '') ||
            // post hash invalid?
            ($bt['pHash'] != $this->getFormPostHash( $bt['ts'], $bt['rnd'] )) ||
            // minimal submission time not reached?
            (
                ( $this->getMsTstamp() - $bt['ts'] ) <
                $this->settings['minFormSubmissionTime']
            ) ||
            // post hash not new?
            $this->entryRepository->countByPostHash( $bt['pHash'] ) ||
            // math test failed?
            (
                $this->settings['useMathTest'] &&
                ( $this->getSecretHash($bt['mts']) != $bt['mtsh'])
            )
        );
    }

    private function getFormPostHash( $timestamp, $random )
    {
        return $this->getSecretHash($timestamp.$random);
    }

    private function getMtLabel($n1, $n2)
    {
        return $n1.' + '.$n2.' = ?';
    }

    private function getMtsHash($n1, $n2)
    {
        return $this->getSecretHash($n1 + $n2);
    }

    /**
    * getSetIsSpamHash
    *
    * @param \MMC\MmcGuestbook\Domain\Model\Entry $entry
    * @return string $hash
    */
    private function getSetIsSpamHash( $entry )
    {
        return $this->getSecretHash( $entry->getPostHash().$entry->getCrdate() );
    }

    private function getSecretHash( $str )
    {
        return md5( $str.$this->settings['secret'] );
    }

    private function getMsTstamp()
    {
        return round(microtime(true)*1000);
    }

    private function getScrambledHTML($str)
    {
        return
            '<script type="text/javascript">'.
            'document.write(decodeURIComponent(\''.rawurlencode($str).'\'))'.
            '</script>';
    }

}
