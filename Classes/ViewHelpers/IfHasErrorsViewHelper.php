<?php
namespace MMC\MmcGuestbook\ViewHelpers;

use TYPO3Fluid\Fluid\Core\Rendering\RenderingContextInterface;
use TYPO3Fluid\Fluid\Core\ViewHelper\AbstractViewHelper;

class IfHasErrorsViewHelper extends AbstractViewHelper 
{


    public function initializeArguments()
    {
        $this->registerArgument('propertyName', 'string', 'The property name (e.g. argument name or property name). This can also be a property path (like blog.title)', '');
        $this->registerArgument('then', 'string', 'The output if there was an error', '');
        $this->registerArgument('else', 'string', 'The output if there was no error', '');
    }

    public static function renderStatic(
            array $arguments,
            \Closure $renderChildrenClosure,
            RenderingContextInterface $renderingContext
    ) 
    {
        $propertyName = $arguments['propertyName'];
        $validationResults = $renderingContext->getControllerContext()->getRequest()->getOriginalRequestMappingResults();
        if ($validationResults !== NULL && $property !== '') {
            $validationResults = $validationResults->forProperty($propertyName);
        }
        return $validationResults->hasErrors() ? $arguments['then'] : $arguments['else'];
    }


}
