<?php
namespace MMC\MmcGuestbook\Domain\Repository;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Matthias Mächler <maechler@mm-computing.ch>, mm-computing.ch
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
* The repository for Entries
*/
class EntryRepository extends \TYPO3\CMS\Extbase\Persistence\Repository 
{

	protected $defaultOrderings = array(
		'crdate' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_DESCENDING,
		'lastname' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING,
		'firstname' => \TYPO3\CMS\Extbase\Persistence\QueryInterface::ORDER_ASCENDING
	);

	public function initializeObject()
	{
		$this->defaultQuerySettings = $this->createQuery()->getQuerySettings()
			->setEnableFieldsToBeIgnored(['disabled'])
			->setRespectSysLanguage(false)
			->setLanguageOverlayMode(false)
			->setIgnoreEnableFields( false );
		$this->setDefaultQuerySettings( $this->defaultQuerySettings );
	}

	public function setFindDisabled( $findDisabled )
	{
        $this->defaultQuerySettings->setIgnoreEnableFields( $findDisabled );
		$this->setDefaultQuerySettings( $this->defaultQuerySettings );
	}

	public function findAllLimitedNotHidden( $offset, $limit )
	{
	  $this->setFindDisabled( false );
		$query = $this->createQuery();
		return $query
			->setOffset( $offset )
			->setLimit( $limit )
			->execute();
	}

	public function findByUidIncludeHidden( $uid )
	{
		$this->setFindDisabled( true );
		$query = $this->createQuery();
		$query->matching( $query->equals('uid', $uid) );
		$r = $query->execute();
		return count($r) ? $r[0] : NULL;
	}

	public function isSpamIp( $ip )
	{
		$this->setFindDisabled( true );
		$query = $this->createQuery();
		$query->matching(
			$query->logicalAnd(
				$query->equals('remoteIp', $ip),
				$query->equals('isSpam', true)
			)
		);
		return ($query->count() > 0);
	}

	public function persistAll()
	{
		$this->persistenceManager->persistAll();
	}

}
