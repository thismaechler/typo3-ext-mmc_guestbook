<?php
namespace MMC\MmcGuestbook\Domain\Model;

/***************************************************************
 *
 *  Copyright notice
 *
 *  (c) 2015 Matthias Mächler <maechler@mm-computing.ch>, mm-computing.ch
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

/**
* Guestbook Entry
*/
class Entry extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity 
{

    /**
    * Date/Time
    *
    * @var integer
    */
    protected $crdate;

    /**
    * Hidden
    *
    * @var boolean
    */
    protected $hidden;

    /**
    * First name
    *
    * @var string
    * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
    */
    protected $firstname = '';

    /**
    * Last name
    *
    * @var string
    *  @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
    */
    protected $lastname = '';

    /**
    * Email
    *
    * @var string
    * @TYPO3\CMS\Extbase\Annotation\Validate("RegularExpression", options={"regularExpression": "/^$|(\S+@\S+\.\S+)$/"})
    */
    protected $email = '';

    /**
    * Website
    *
    * @var string
    */
    protected $url = '';

    /**
    * City / place
    *
    * @var string
    */
    protected $place = '';

    /**
    * Message
    *
    * @var string
    * @TYPO3\CMS\Extbase\Annotation\Validate("NotEmpty")
    */
    protected $message = '';

    /**
    * Comment to the message
    *
    * @var string
    */
    protected $comment = '';

    /**
    * remoteIp
    *
    * @var string
    */
    protected $remoteIp = '';

    /**
    * postHash
    *
    * @var string
    */
    protected $postHash = '';

    /**
    * isSpam
    *
    * @var boolean
    */
    protected $isSpam = false;


    /**
    * Returns the crdate
    *
    * @return integer $crdate
    */
    public function getCrdate() 
    {
        return $this->crdate;
    }

    /**
    * Sets the crdate
    *
    * @param integer $crdate
    * @return void
    */
    public function setCrdate($crdate) 
    {
        $this->crdate = $crdate;
    }

    /**
    * Returns hidden
    *
    * @return boolean $hidden
    */
    public function getHidden() 
    {
        return $this->hidden;
    }

    /**
    * Sets hidden
    *
    * @param boolean $hidden
    * @return void
    */
    public function setHidden($hidden) 
    {
        $this->hidden = $hidden;
    }

    /**
    * Returns the firstname
    *
    * @return string $firstname
    */
    public function getFirstname() 
    {
        return $this->firstname;
    }

    /**
    * Sets the firstname
    *
    * @param string $firstname
    * @return void
    */
    public function setFirstname($firstname) 
    {
        $this->firstname = $firstname;
    }

    /**
    * Returns the lastname
    *
    * @return string $lastname
    */
    public function getLastname() 
    {
        return $this->lastname;
    }

    /**
    * Sets the lastname
    *
    * @param string $lastname
    * @return void
    */
    public function setLastname($lastname) 
    {
        $this->lastname = $lastname;
    }

    /**
    * Returns the email
    *
    * @return string $email
    */
    public function getEmail() 
    {
        return $this->email;
    }

    /**
    * Sets the email
    *
    * @param string $email
    * @return void
    */
    public function setEmail($email) 
    {
        $this->email = $email;
    }

    /**
    * Returns the url
    *
    * @return string $url
    */
    public function getUrl() 
    {
        return $this->url;
    }

    /**
    * Sets the url
    *
    * @param string $url
    * @return void
    */
    public function setUrl($url) 
    {
        $this->url = $url;
    }

    /**
    * Returns the place
    *
    * @return string $place
    */
    public function getPlace() 
    {
        return $this->place;
    }

    /**
    * Sets the place
    *
    * @param string $place
    * @return void
    */
    public function setPlace($place) 
    {
        $this->place = $place;
    }

    /**
    * Returns the message
    *
    * @return string $message
    */
    public function getMessage() 
    {
        return $this->message;
    }

    /**
    * Sets the message
    *
    * @param string $message
    * @return void
    */
    public function setMessage($message) 
    {
        $this->message = $message;
    }

    /**
    * Returns the comment
    *
    * @return string $comment
    */
    public function getComment() 
    {
        return $this->comment;
    }

    /**
    * Sets the comment
    *
    * @param string $comment
    * @return void
    */
    public function setComment($comment) 
    {
        $this->comment = $comment;
    }

    /**
    * Returns the remoteIp
    *
    * @return string $remoteIp
    */
    public function getRemoteIp() 
    {
        return $this->remoteIp;
    }

    /**
    * Sets the remoteIp
    *
    * @param string $remoteIp
    * @return void
    */
    public function setRemoteIp($remoteIp) 
    {
        $this->remoteIp = $remoteIp;
    }

    /**
    * Returns the postHash
    *
    * @return string $postHash
    */
    public function getPostHash() 
    {
        return $this->postHash;
    }

    /**
    * Sets the postHash
    *
    * @param string $postHash
    * @return void
    */
    public function setPostHash($postHash) 
    {
        $this->postHash = $postHash;
    }

    /**
    * Returns isSpam
    *
    * @return boolean $isSpam
    */
    public function getIsSpam() 
    {
        return $this->isSpam;
    }

    /**
    * Sets isSpam
    *
    * @param boolean $isSpam
    * @return void
    */
    public function setIsSpam($isSpam) 
    {
        $this->isSpam = $isSpam;
    }

}
