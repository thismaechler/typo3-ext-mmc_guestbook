<?php

$EM_CONF[$_EXTKEY] = [
    'title' => 'MMC Guestbook',
    'description' => 'A guestbook plugin providing several mechanisms for spam prevention',
    'category' => 'plugin',
    'author' => 'Matthias Mächler',
    'author_email' => 'maechler@mm-computing.ch',
    'state' => 'stable',
    'uploadfolder' => false,
    'version' => '3.0.5',
    'constraints' => [
        'depends' => [
            'typo3' => '11.5.0-11.5.99',
        ],
        'conflicts' => [],
        'suggests' => [],
    ]
];
